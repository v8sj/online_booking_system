<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------

namespace app\index\controller;
error_reporting(0);
use library\Controller;
use library\tools\Data;
use think\Db;
/**
 * 应用入口
 * Class Index
 * @package app\index\controller
 */
class Index extends Controller
{
	public $maxNum =1000;

    public $phoneMax =5;

	function _initialize()
	{
		$this->assign('maxNum' , $this->maxNum);
	}


    /**
     * 入口链接
     */
    public function index()
    {

        $this->fetch();
    }




    public function list()
    {
        $key   = $this->request->param('key');
        if ($key !='admin888') {
            echo '错误！';
            die();
        }


        if ($this->request->isAjax()) {

            $where  = $data = [];
            $page   = $this->request->param('page/d', 1);
            $limit  = $this->request->param('limit/d', 15);
            $title    = $this->request->param('title');
            $visitdate    = $this->request->param('visitdate');
            if ($title) {
                //$where[] = ['name|phone|idcard', 'like', "%{$title}%"];
                $where[] = ['name|phone|idcard' , 'LIKE', '%' . $title . '%'];
            }
            if ($visitdate) {
                //$where['visitdate'] = $visitdate;
                $where[]    = ['visitdate', 'eq', $visitdate];
            }
            
            $data['code']   = 0;
            $data['msg']    = '';
            $data['count']  = Db::table('visitor')->where($where)->count('id');
            $data['data']   = Db::table('visitor')->where($where)->page($page)->order('id desc')->limit($limit)->select();
            if (count($data['data']) >=1) {
                foreach ($data['data'] as $key => $v) {
                    //$data['data'][$key]['name'] =dataDesensitization($v['name'], 1, 1);
                    if ($v['phone'] !='') {
                        $data['data'][$key]['phone'] =dataDesensitization($v['phone'], 3, 4);
                    }
                    if ($v['idcard'] !='') {
                        $data['data'][$key]['idcard'] =dataDesensitization($v['idcard'], 5, 8);
                    }
                }
            }

            //echo count($data['data']) ;


            echo json_encode($data ,true);
            die();
        }

        $this->fetch();
    }





    public function console()
    {
        $today =date('Y-m-d' ,time());
        $data1 =Db::table('visitor')->where(['visitdate'=>$today])->count();
        $undata1 = $this->maxNum-$data1;
        
        //2020-03-05 - 2020-03-06
        $day7 = date('Y-m-d' ,time()+24*3600*7);
        $list = Db::table('visitor')->field('visitdate, count(`id`) as c')
        ->whereBetweenTime('visitdate', $today, $day7)
        ->group('visitdate')
        ->select();


        $morning = Db::table('visitor')->where(['visitdate'=>$today , 'datez'=>'上午'])->count();
        $afternoon = Db::table('visitor')->where(['visitdate'=>$today , 'datez'=>'下午'])->count();
        #print_r($afternoon);


        $this->assign('list' , $list);
        $this->assign('today' , $today);
        $this->assign('data1' , $data1);
        $this->assign('undata1' , $undata1);
        $this->assign('morning' , $morning);
        $this->assign('afternoon' , $afternoon);

    	$this->fetch();
    }


    public function addappointment()
    {
    	//list($data, $string) = [$this->request->get(), []];
        $data =$this->request->param();
    	if (count($data) ==0  ) {
            $data['visitdate'] =date('Y-m-d' , time());
    	}else{
            $data['visitdate'] =$data['visitdate'];
        }

        $num = Db::table('visitor')->where(['visitdate'=>$data['visitdate'] ])->count();
        if ($num >= $this->maxNum) {
            $this->error($data['visitdate'] .'预约已满！');
        }


		$this->assign('today' , date('Y-m-d' ,time()) );
    	$this->assign('visitor' , $data['visitdate']);
    	$this->fetch();
    }


    public function save()
    {
    	if ($this->request->isPost()) {
    		list($data, $string) = [$this->request->post(), []];

    		$data['adddate'] =date('Y-m-d' , time() );
    		$data['person'] =intval($data['person']);


	    	$num = Db::table('visitor')->where(['visitdate'=>$data['visitdate'] ])->count();
	    	if ($num >= $this->maxNum) {
	    		$this->error($data['visitdate'] .'预约已满！');
	    	}

    		$rs =Db::table('visitor')->where(['visitdate'=>$data['visitdate'],'phone'=>$data['phone'] ])->count();
    		if ($rs == $this->phoneMax ) {
    			$rst =[];
    			$rst['code'] =401;
    			$rst['msg'] ='预约失败，一个手机号码24小时只能预约5人次！';
    			echo json_encode($rst);
    			die();
    		}

    		$ids =Db::name('visitor')->strict(false)->insertGetId($data);
    		if ($ids > 0) {
    			$rst =[];
    			$rst['code'] =200;
    			$rst['msg'] ='预约成功';
    			echo json_encode($rst);
    		}else{
    			$rst =[];
    			$rst['code'] =400;
    			$rst['msg'] ='预约失败';
    			echo json_encode($rst);
    		}

    	}
    }



    public function myappointment()
    {
    	$this->fetch();
    }


    public function getcount()
    {
        if ($this->request->isPost()) {
            list($data, $string) = [$this->request->post(), []];

            $total =Db::table('visitor')->where(['visitdate'=>$data['visitdate'] ])->count();
            if ($total >= $this->maxNum) {
                $rst =[];
                $rst['code'] =400;
                $rst['msg'] = '今天的预约已经满了，请预约其他日期';
                echo json_encode($rst);
                die();
            }

            $sql ="select datez , count(`id`) as cc from visitor where visitdate ='".$data['visitdate']."' group BY datez";
            $rs =Db::table('visitor')->query($sql);

            $morning = 400 -$rs['0']['cc'];
            $afternoon = 600 -$rs['0']['cc'];
            $msg .=$data['visitdate'] . '上午还可预约数'.$morning;
            $msg .='，下午还预约数'.$afternoon;

            $rst =[];
            $rst['code'] =200;
            $rst['msg'] = $msg;
            echo json_encode($rst);
        }
    }





    public function getdatez()
    {
        if ($this->request->isPost()) {
            list($data, $string) = [$this->request->post(), []];

            $count =Db::table('visitor')->where(['datez'=>$data['datez'],'visitdate'=>$data['visitdate'] ])->count();

            if($data['datez'] =='上午' && intval($count) >= 400){
                $msg =$data['datez'].'的400个号已经预约满了。';
                $rst['code'] =400;
                $rst['msg'] = $msg;
                echo json_encode($rst);
            }elseif ($data['datez'] =='下午' && intval($count) >= 600) {
                $msg =$data['datez'].'的600个号已经预约满了。';
                $rst['code'] = 400;
                $rst['msg'] = $msg;
                echo json_encode($rst);
            }else{
                $msg ='OK';
                $rst['code'] = 200;
                $rst['msg'] = $msg;
                echo json_encode($rst);     
            }
        }
    }





    public function query()
    {
    	if ($this->request->isGet()) {
    		list($data, $string) = [$this->request->get(), []];
    		if ($data['title'] !='' ) {
    			$rs =Db::table('visitor')->where(['name|phone'=>$data['title'],'visitdate'=>$data['visitdate'] ])->find();

    			if ($rs) {
    				$today =date('Y-m-d' ,time());
    				if ($rs['visitdate'] < $today) {
    					$msg ='预约已经过期！';
    				}else{
    					$msg ='预约成功!';
    				}

                    $rs['phone'] =dataDesensitization($rs['phone'], 3, 4);
                    $rs['idcard'] =dataDesensitization($rs['idcard'], 5, 8);

    				$this->assign('msg' , $msg);
    				$this->assign('vo' , $rs);
    				$this->fetch();
    			}else{
    				echo '<br />没有找到预约记录！<br />';

    				echo '<a href="/index/index/addappointment">前往预约</a> 或 <a href="/index/index/myappointment">重新查询</a>';
    			}

    		}
    	}
    }



}
