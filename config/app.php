<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------

return [
    // 应用调试模式
    'app_debug'      => true,
    // 应用Trace调试
    'app_trace'      => false,
    // 0按名称成对解析 1按顺序解析
    'url_param_type' => 1,
    // 当前 ThinkAdmin 版本号
    'thinkadmin_ver' => 'v5',
	

    //电子签名配置
    'signature'=>[
        'path'=> 'uploads/signature/',  //上传目录配置（相对于根目录）
        'url'=>'/uploads/signature/',  //url（相对于web目录）
        'validate'=>[
            //默认不超过50mb
            'size' => 52428800,
            //常用后缀
            'ext'  => 'jpg,jpeg,png,gif',
        ],
        
    ],


	
    //附件配置
    'attchment'=>[
        'path'=> 'uploads/attachment/',  //上传目录配置（相对于根目录）
        'url'=>'/uploads/attachment/',  //url（相对于web目录）
        'validate'=>[
            //默认不超过50mb
            'size' => 52428800,
            //常用后缀
            'ext'  => 'bmp,ico,psd,jpg,jpeg,png,gif,doc,docx,xls,xlsx,pdf,zip,rar,7z,tz,mp3,mp4,mov,swf,flv,avi,mpg,ogg,wav,flac,ape,txt',
        ],
    ]
	
];
