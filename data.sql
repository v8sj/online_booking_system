/*
Navicat MySQL Data Transfer

Source Server         : 11
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : zhiwuyuan

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-03-10 21:17:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `visitor`
-- ----------------------------
DROP TABLE IF EXISTS `visitor`;
CREATE TABLE `visitor` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `visitdate` date DEFAULT NULL,
  `datez` varchar(10) DEFAULT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `person` mediumint(3) DEFAULT '0',
  `idcard` varchar(18) DEFAULT NULL,
  `remark` varchar(250) DEFAULT NULL,
  `adddate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1013 DEFAULT CHARSET=utf8mb4;
