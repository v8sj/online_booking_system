## 系统介绍

网络预约系统，采用thinkphp5.1 + mysql开发，界面友好，人性化，
二次开发简单，可适用于公园，博物馆，图书馆等场景，二次甚至可以预约考试，医院门诊等等。
能兼容pc，手机，微信客户端，平板终端等。

1. 本项目码云上的免费开源，可以免费使用，请勿在其他平台出卖或商业化，一经发现，必究法律责任！
1. 项目最终解释权归作者开发者。

## 运行架构


在php5.6以上，apache2.2,mysql5.6下配置，建议使用phpstudy,宝塔面板，wamp这样的php套件

##  下载安装
码云地址：https://gitee.com/fsscms/online_booking_system

1.下载后解压到服务器目录，然后在目录运行

`composer install`

2.在服务器上新建站点，将域名指向public目录

3.登陆mysql，新建数据库，将sql脚本导入数据库

4.修改config/databse.php的mysql配置。

5.打开浏览器，输入你的域名，开始你的旅程。

## 联系方式
QQ: 80392625
mail: win-phpl@163.com

##  演示，打开微信扫一扫下面的二维码，进行快乐玩耍


![](https://images.gitee.com/uploads/images/2020/0310/210656_8e8e4ef7_5744836.png)


![](https://images.gitee.com/uploads/images/2020/0310/210655_d0bc87a1_5744836.jpeg)

![](https://images.gitee.com/uploads/images/2020/0310/210655_9d4527ca_5744836.jpeg)

![](https://images.gitee.com/uploads/images/2020/0310/210655_78e4f944_5744836.jpeg)

![](https://images.gitee.com/uploads/images/2020/0310/210655_8cdffb69_5744836.jpeg)

![](https://images.gitee.com/uploads/images/2020/0310/210655_24022636_5744836.jpeg)

![](https://images.gitee.com/uploads/images/2020/0310/210655_fae215d4_5744836.jpeg)

![](https://images.gitee.com/uploads/images/2020/0310/210655_f540d64d_5744836.jpeg)


